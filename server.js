console.log("Arrancando la consola nodeJS");

require('dotenv').config(); //con esto cargamos el fichero .env y recupera así las varieables globales. Lo añade en el objeto "process.env"
const express = require('express'); //cargamos en esta constante el módulo de express Porque así lo define el frameworf
const app = express(); //inicializar el framework de express
app.use(express.json());//para poder utilizar json, se actualiza esta variable. hace el json.parse internaente.

const port= process.env.PORT || 3000; //inicializamos el puerto si no hay otro puerto por defecto escuchamos en el 3000 (PORT es una variable que tienes definida sino coge el 3000)



const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

//esto es para q el CORS del front pueda ejectar la petición.
var enableCORS = function(req, res, next){
  // por esta función van a pasar todas las peticiones desde el front.
  //esto va a añadir a las cabeceras un tema para que valide que la petición es correcta.
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
//para permitir incluit nuestra cabecera que viene del front. Si queremos usar otro api se pone aqií
  res.set("Access-Control-Allow-Headers", "Content-Type");

  next();

}// end enableCORS

app.use(enableCORS);

app.listen(port);

console.log("API escuchando en el puerto " + port);




//usuarios nuestra primera entidad.  Los datos están en un fichero.
//pasamos la función, realmente no se la está llamando.

app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);
app.post("/apitechu/v2/users", userController.createUserV2);
app.put("/apitechu/v2/users/:id", userController.deleteUsersByIdV2); //borramos un usuario de mongo

app.get("/apitechu/account/:id", accountController.getAccountByUser);

app.get("/apitechu/v1/users", userController.getUsersV1);
// HAcemos un post en users.
app.post("/apitechu/v11/users",userController.createUserV11);

// HAcemos un post en users y recogemos del body
app.post("/apitechu/v1/users", userController.createUserV1 );

// HAcemos un delete en users.El identificados se recupera de parametros
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);




//PRACTICA USUARIO LOGIN
// HAcemos un post en users.
app.post("/apitechu/v1/login",authController.login);
app.post("/apitechu/v2/login",authController.loginV2);

// HAcemos un post en users.
app.post("/apitechu/v1/logout/:id",authController.logout);
app.post("/apitechu/v2/logout/:id",authController.logoutV2);


// con este código generamos una repuesta que puede ser invocada desde otro lado (POSTMAN)
//app es un objeto de express. Se le pueden añadir cualquier método get put...
// app.get tiene dos parámetros en el primero definimos el nombre, en el segundo es una función que hace lo que queramos.
// en este caso definimos una función (podría estar fuera y añadir aquí la variable sobre la que lo ejecutamos la función)
// el req es para recuperar el requiest y el res la respuesta.
app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    //se le puede mandar un texto con fonrmato json. EN este caso por defcto lo entoende como html aunque se puede ver tb json
    var respuesta = '{"msg" : "Hola campeón. Has contactado con APITECHU"};';


    //le mandas directamente un json por lo tanto el post entiende que es json y te lo abre así.
    // Debe de ser así para que sepa q lo es.
    res.send({"msg" : "Hola campeón. Has contactado con APITECHU"});
  }
)

//mandamos parametros. Si se mandan parametros por la url en la defición tiene que estar para recogerlos
//Los queryString van a partir de la ? no se definen
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("parametros ");
    console.log(req.params);

    console.log("queryString ");
    console.log(req.query);

    console.log("headers ");
    console.log(req.headers);

// hay q decirle que lo considere como json lo ponemos al principio
    console.log("body ");
    console.log(req.body);




  }
)


// Código del profe para lo mismo que hice yo en el de antes
// app.get("/apitechu/v1/users",
//  function getUsersV1(req, res) {
//    console.log("GET /apitechu/v1/users");
//
//    var result = {};
//    var users = require('./usuarios.json');
//
//    if (req.query.$count == "true") {
//      console.log("Count needed");
//      result.count = users.length;
//    }
//
//    result.users = req.query.$top ?
//      users.slice(0, req.query.$top) : users;
//
//    res.send(result);
//  }
// )
