const fs = require('fs'); //

//funcion para acceso al fichero q se usa tanto para grabar como para border-primary
function writeUserDataToFile(data){
    var jsonUserData = JSON.stringify(data);
    //hace la escritura

    // Parametros: 1.nombre del fichero donde se quiere guardar 2. datos 3. Encoding 4.Control de la salida.
    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function (err){
      if (err) {
        console.log("err: " + err);
      } else {
      console.log("Se ha guardado el registro");
      }
    }
  );

}//fin writeUserDataToFile


module.exports.writeUserDataToFile = writeUserDataToFile; //para poder llamarla desde fuera. Se expone para que se invoque desde otro fichero usando module.exports.writeUserDataToFile
