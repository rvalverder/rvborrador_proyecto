#Dockerfile

#Imagen raiz de la que partimos  [node de nodeJS]
FROM node

#Carpeta raiz, donde va a descargar el contenido cuando se instale.
WORKDIR /apitechu

#Copia el código fuente de nuestra api a imagen. Añadimos desde el raiz . a algo que existe en la imagen en /apitechu
ADD . /apitechu

# Instalación de las dependencias. Las dependencias no forman parte del proyecto. En el proceso de construcción de la img se descargaran
# queremos que meta solo las dependencias definidas en dependencias de nuestro proyecto
RUN npm install --only=prod

#exposición del puerto
EXPOSE 3000

#COMANDO DE INICIALIZACIÓN
CMD ["node", "server.js"]
