const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should(); //se usa para hacer las aserciones. Método que tiene definido chai

//para crear agupación de test
describe("First test",
  function(){
    it("Test that DuckDuckGo works" , function(done){
      chai.request('http://www.duckduckgo.com')
      .get('/')
      .end(
        function(err, resp){
          console.log("Request finished");
          //console.log(resp);
          //console.log(err);
          resp.should.have.status(200);//es la comprobación que queremos hacer, q el estado de la petición es 200, esto es que va ok
          done();//indica cuando tiene que comprobar el framework
        }//fin function(err, resp)
      )//fin end
    }//fin function(done)
   )//fin it ("Test that DuckDuckGo works"
 }//fin function()

)//fin describe





//para crear agupación de test
describe("Test de API Usuarios",
  function(){
    it("Pueba API responde" , function(done){
      chai.request('http://localhost:3000')
      .get('/apitechu/v1/hello')
      .end(
        function(err, resp){
          console.log("Request finished");
          resp.should.have.status(200);//es la comprobación que queremos hacer, q el estado de la petición es 200, esto es que va ok
          resp.body.msg.should.be.eql("Hola campeón. Has contactado con APITECHU");//para comprobar el mensaje que manda. msg es lo que la función que comprobamos mete en el body
          done();//indica cuando tiene que comprobar el framework
        }//fin function(err, resp)
      )//fin end
    }//fin function(done)
  ),//fin it se pueden añadir mas IT
  it("Prueba API lista de usuarios correctos" , function(done){
    chai.request('http://localhost:3000')
    .get('/apitechu/v1/users')
    .end(
      function(err, resp){
        console.log("Comprobar usuarios correctos");
        resp.should.have.status(200);//es la comprobación que queremos hacer, q el estado de la petición es 200, esto es que va ok
        resp.body.should.be.a("array");//para comprobar que es un array en la respuesta. SI la respuesta estuviera dentre de un objeto harbía que poneroresp.body.usuarios.should.be.a("array");

        for(user of resp.body)
        {
          user.should.have.property('first_name');
          user.should.have.property('email');
        }

        done();//indica cuando tiene que comprobar el framework
      }//fin function(err, resp)
    )//fin end
  }//fin function(done)
  )

 }//fin function(),


)//fin describe
