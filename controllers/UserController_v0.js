const io = require('../io'); // con esto cargamos el fichero io.js donde estáran nuestras funciones (no hace falta el .js se da por hecho)
//require por defecto mira en node_modules, por eso se pone ./ para sacarlo de ahí.



function getUsersV1(req, res){
  console.log("GET /apitechu/v1.1/users");

  //definimos una variable con la ruta del fichero y lo cargamos en la variables users
  var users = require('../usuarios.json'); //require es una función que recupera y carga el fichero.

  //recuperamos el parámetro enviado
  console.log("queryString " + req.query);
  var top = req.query.$TOP;
  var count = req.query.$COUNT;
  var totalReg = 0;

  //Valor del top recuperado
  console.log("numero de registros a recuperar: " + top);
  console.log("contador: " + count);

  // Recuperamos el total del objeto
    totalReg = users.length;
    console.log("Con contador " + totalReg);

  // con slice recuperamos el subconjunto de registros que nos piden
  var userFiltro = users.slice(0,top);

  //la respuesta por defecto todo
  var respuesta = users;

  if (count == "true" && top)
  {
    respuesta = {"usuarios" : userFiltro, "contador" : totalReg};
  }
  else if (top )
  {
    respuesta = {"usuarios" : userFiltro};
  }
  else if (count == "true") {
    respuesta = {"contador" : totalReg};
  }

  //Mandamos el objeto a la respuesta
  res.send(respuesta);


}



function createUserV11(req, res){
  console.log("put /apitechu/v1/users");

  //recuperamos la cabecera del postman o del donden nos llamen
  console.log(req.headers);

  console.log("Hay que dar de alta este registro: " + req.headers.first_name + "/" + req.headers.last_name + "/" + req.headers.email);

  //objeto que contiene los valores recibidos
  var newUser = {
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "email" : req.headers.email
  };

  var users = require('../usuarios.json'); //recupero el fichero que contiene los usuarios.
  users.push(newUser); //se añade el nuevo objeto al objeto users

  console.log("Usuario añadido al array");

  const fs = require('fs'); //

  //quiero guardar el contenido del objeto en formato json no el objeto tal cualquier
  var jsonUserData = JSON.stringify(users);

  //hace la escritura
  // Parametros: 1.nombre del fichero donde se quiere guardar 2. datos 3. Encoding 4.Control de la salida.
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
  function (err){
    if (err) {
      console.log("err: " + err);
    } else {
    console.log("Se ha guaardado el registro");
    }
  }
);

res.send("Alta de usuario realizado con éxito");
}


function createUserV1(req, res){
  console.log("post/apitechu/v2/users");

  //objeto que contiene los valores recibidos
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require('../usuarios.json'); //recupero el fichero que contiene los usuarios.
  users.push(newUser); //se añade el nuevo objeto

  console.log("Usuario añadido al array");

  //Refactorización para invocar a la función en lugar de ejecutar el código
  //trozo de código para recuperar el fichero y reescribirlo
//   const fs = require('fs'); //
//
//
//   var jsonUserData = JSON.stringify(users);
//
//   //hace la escritura
//   // Parametros: 1.nombre del fichero donde se quiere guardar 2. datos 3. Encoding 4.Control de la salida.
//   fs.writeFile("./usuarios.json", jsonUserData, "utf8",
//   function (err){
//     if (err) {
//       console.log("err: " + err);
//     } else {
//     console.log("Se ha guardado el registro");
//     }
//   }
// );

//en lugar de todo el código como se ha generado una función se puede llamar directametne a ella y no hace falta todo l demas.

io.writeUserDataToFile(users); //Se llama a la función grabar usando el objeto io que instanciamos al principio.
console.log("Fin alta de usuarios");
res.send("Alta de usuario realizado con éxito");

}


function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');

  var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  var  usuarioBorrado = "";
  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.id + " y " +   req.params.id);
      usuarioBorrado = element.id + " - "+ element.first_name + " " + element.last_name + ".";
      return element.id == req.params.id
    }
  )

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    users.splice(indexOfElement, 1);
    deleted = true;
  }

  if (deleted) {
    io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado " + usuarioBorrado : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}



module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV11 = createUserV11;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
