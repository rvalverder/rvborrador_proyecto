const requestJson = require('request-json');
const io = require('../io'); // con esto cargamos el fichero io.js donde estáran nuestras funciones (no hace falta el .js se da por hecho)
//require por defecto mira en node_modules, por eso se pone ./ para sacarlo de ahí.

//url base que voy a usar para ejecutar con la base de datos
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurvr11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //asignamos el del api key que recuperamos de process.env (variables globales)

//módulo de encriptado. Lo cargamos
const crypt = require('../crypt');



function getAccountByUser(req, res){
  console.log("GET/apitechu/account/:id");

  var id = req.params.id;
  var query = 'q={"userId":' + id + '}';
  console.log("consulta " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");

  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
        if(err) {
          var response = {"msg" : "Error obteniendo usuarios"}
          res.status(500);
        } else {
          if (body.length > 0){
            var response = body;
          }
          else {
            var response = {"msg" : "Usuario no encontrado"};
            res.status(404);
          }

        }
               res.send(response);
     }
     //body[0] mandamos el elemento 0 ya q solo recuperamos 1 registro


  )


}//function getUsersByIdV2


module.exports.getAccountByUser = getAccountByUser;
