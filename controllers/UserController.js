//Con conexión a la bse de datos

const requestJson = require('request-json');
const io = require('../io'); // con esto cargamos el fichero io.js donde estáran nuestras funciones (no hace falta el .js se da por hecho)
//require por defecto mira en node_modules, por eso se pone ./ para sacarlo de ahí.

//url base que voy a usar para ejecutar con la base de datos
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurvr11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //asignamos el del api key que recuperamos de process.env (variables globales)

//módulo de encriptado. Lo cargamos
const crypt = require('../crypt');


function getUsersV1(req, res){
  console.log("GET /apitechu/v1.1/users");

  //definimos una variable con la ruta del fichero y lo cargamos en la variables users
  var users = require('../usuarios.json'); //require es una función que recupera y carga el fichero.

  //recuperamos el parámetro enviado
  console.log("queryString " + req.query);
  var top = req.query.$TOP;
  var count = req.query.$COUNT;
  var totalReg = 0;

  //Valor del top recuperado
  console.log("numero de registros a recuperar: " + top);
  console.log("contador: " + count);

  // Recuperamos el total del objeto
    totalReg = users.length;
    console.log("Con contador " + totalReg);

  // con slice recuperamos el subconjunto de registros que nos piden
  var userFiltro = users.slice(0,top);

  //la respuesta por defecto todo
  var respuesta = users;

  if (count == "true" && top)
  {
    respuesta = {"usuarios" : userFiltro, "contador" : totalReg};
  }
  else if (top )
  {
    respuesta = {"usuarios" : userFiltro};
  }
  else if (count == "true") {
    respuesta = {"contador" : totalReg};
  }

  //Mandamos el objeto a la respuesta
  res.send(respuesta);


}



function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + mLabAPIKey,
     function(err, resMLab, body) {
       var response = !err ? body : {
         "msg" : "Error obteniendo usuarios"
     }

       res.send(response);
     }
  )
}



function getUsersByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("consulta " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
        if(err) {
          var response = {"msg" : "Error obteniendo usuarios"}
          res.status(500);
        } else {
          if (body.length > 0){
            var response = body[0];
          }
          else {
            var response = {"msg" : "Usuario no encontrado"};
            res.status(404);
          }

        }
               res.send(response);
     }
     //body[0] mandamos el elemento 0 ya q solo recuperamos 1 registro


  )
}//function getUsersByIdV2



function createUserV11(req, res){
  console.log("put /apitechu/v1/users");

  //recuperamos la cabecera del postman o del donden nos llamen
  console.log(req.headers);

  console.log("Hay que dar de alta este registro: " + req.headers.first_name + "/" + req.headers.last_name + "/" + req.headers.email);

  //objeto que contiene los valores recibidos
  var newUser = {
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "email" : req.headers.email
  };

  var users = require('../usuarios.json'); //recupero el fichero que contiene los usuarios.
  users.push(newUser); //se añade el nuevo objeto al objeto users

  console.log("Usuario añadido al array");

  const fs = require('fs'); //

  //quiero guardar el contenido del objeto en formato json no el objeto tal cualquier
  var jsonUserData = JSON.stringify(users);

  //hace la escritura
  // Parametros: 1.nombre del fichero donde se quiere guardar 2. datos 3. Encoding 4.Control de la salida.
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
  function (err){
    if (err) {
      console.log("err: " + err);
    } else {
    console.log("Se ha guaardado el registro");
    }
  }
);

res.send("Alta de usuario realizado con éxito");
}

function createUserV2 (req, res) {
console.log("post/apitechu/v2/users este de aquñi bne");
console.log("id:" + req.body.id);
console.log("first_name:" + req.body.first_name);
console.log("last_name:" + req.body.last_name);
console.log("email:" + req.body.email);
console.log("password:" + req.body.password);

//encriptar la contraseña.


var newUser = {"id" : req.body.id,
"first_name" : req.body.first_name,
"last_name" : req.body.last_name,
"email" : req.body.email,
"password" : crypt.hash(req.body.password)
}

console.log(newUser);

var httpClient = requestJson.createClient(baseMLabURL);
httpClient.post("user?" + mLabAPIKey, newUser,
  function(err, resMLab, body){
    console.log("usuario creado");
    res.status(201).send({"msg" : "usuario creado"});

  }

)

}


function deleteUsersByIdV2(req, res){
  console.log("DELETE /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("consulta " + query);

  var httpClient = requestJson.createClient(baseMLabURL);

  //para borrar por el id hay que pasarle el id y el body vacio [] o lo recuperamos y creamos vacío o cfreamos objeto vacío
  var bodyPut = [];

   httpClient.put("user?" + query + "&" + mLabAPIKey, bodyPut,
      function(err, resMLab, body) {
        console.log("se borra usuario ");

        body.removed == 0 ? res.status(401).send({"msg" : "Usuario no encontrado" }) : res.status(200).send({"msg" : "Usuario: "+ id + " eliminado correctamente" });
      }
   )

}//function delete




function createUserV1(req, res){
  console.log("post/apitechu/v1/users");

  //objeto que contiene los valores recibidos
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require('../usuarios.json'); //recupero el fichero que contiene los usuarios.
  users.push(newUser); //se añade el nuevo objeto

  console.log("Usuario añadido al array");

  //Refactorización para invocar a la función en lugar de ejecutar el código
  //trozo de código para recuperar el fichero y reescribirlo
//   const fs = require('fs'); //
//
//
//   var jsonUserData = JSON.stringify(users);
//
//   //hace la escritura
//   // Parametros: 1.nombre del fichero donde se quiere guardar 2. datos 3. Encoding 4.Control de la salida.
//   fs.writeFile("./usuarios.json", jsonUserData, "utf8",
//   function (err){
//     if (err) {
//       console.log("err: " + err);
//     } else {
//     console.log("Se ha guardado el registro");
//     }
//   }
// );

//en lugar de todo el código como se ha generado una función se puede llamar directametne a ella y no hace falta todo l demas.

io.writeUserDataToFile(users); //Se llama a la función grabar usando el objeto io que instanciamos al principio.
console.log("Fin alta de usuarios");
res.send("Alta de usuario realizado con éxito");

}


function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');

  var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  var  usuarioBorrado = "";
  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.id + " y " +   req.params.id);
      usuarioBorrado = element.id + " - "+ element.first_name + " " + element.last_name + ".";
      return element.id == req.params.id
    }
  )

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    users.splice(indexOfElement, 1);
    deleted = true;
  }

  if (deleted) {
    io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado " + usuarioBorrado : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}



module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.createUserV11 = createUserV11;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUsersByIdV2 = deleteUsersByIdV2;
