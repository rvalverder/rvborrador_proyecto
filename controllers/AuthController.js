const requestJson = require('request-json');

//Para jugar el fichero
const io = require('../io');
//módulo de encriptado. Lo cargamos
const crypt = require('../crypt');

//url base que voy a usar para ejecutar con la base de datos
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurvr11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //asignamos el del api key que recuperamos de process.env (variables globales)



function login(req, res){
  console.log("post/apitechu/v1/login");

  //recuperamos del body el usuario.
  console.log(req.body.email);
  console.log(req.body.password);
  var existe = false;

  var users = require('../usuarios.json');
  var idUser = "";

  for (var i = 0; i < users.length; i++) {
     console.log("comparando " + users[i].email + " y " +  req.body.email);
     console.log("comparando " + users[i].password + " y " +  req.body.password);

    if (users[i].email == req.body.email && users[i].password == req.body.password) {
      console.log("La posicion " + i + " coincide");
      console.log("código user " + users[i].id);

      idUser = users[i].id;
      users[i].logged = true;
      io.writeUserDataToFile(users);

      existe = true;
      break;
    }
  }

  var msg = existe ?
  "Usuario logado correctamente"  : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg, "idUsuario": idUser});


}


function loginV2(req, res){
  console.log("post/apitechu/v2/login");

  //Recuperar datos del usuario Get(datos del usuario)
  //Comprobar las contraseñas  comprobar contraseña
  //si ok Actualizar put(logeado = true)
  //no ok res.send()

  //recuperamos del body el usuario.
  console.log(req.body.email);
  console.log(req.body.password);
  var existe = false;



  var email = req.body.email;
  var pass = req.body.password;

  var query = 'q={"email":"' + email + '"}';
  console.log("consulta " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
        if(err) {
          var response = {"msg" : "Error obteniendo usuarios"}
          res.status(500);
        } else {
          if (body.length > 0){
            console.log("Voy por aquí");
            var passBBDD = body[0].password;
            console.log("contraseña:"+ passBBDD);
            // Usuario recuperado comparamos las contraseñas
            var id = body[0].id;


            if (crypt.checkPassword(pass, passBBDD)){
              console.log("contraseña ok");
              //Añadir logged = true en la base de datos.
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(err, resMLab, body) {
                    console.log("Añadimos propiedad true ");
                    //res.status(200); No hace falta ponerlo, por defecto es 200
                    var response = { "msg" : "Usuario logado con éxito", "idUsuario" : id}
                    res.send(response);



                })

            }
            else {
              res.status(401);

              res.send({"msg" : "usuario / contraseña incorrecta"});
            }


          }
          else {
            res.status(404);
            res.send({"msg" : "usuarios no encontrado"});
          }

        }
               //res.send(response);
     }
     //body[0] mandamos el elemento 0 ya q solo recuperamos 1 registro


  )




}





function logoutV2(req, res){
  console.log("post/apitechu/v2/logoutV2");

  //Recuperar datos del usuario Get(datos del usuario)
  //Si usuario está logado se quita la propiedad


  //recuperamos del body el usuario.
  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("id:" + id);

   var query = 'q={"id":' + id + '}';
   console.log("consulta " + query);

   var httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente http creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
        if(err) {
          res.status(500);
          res.send({"msg" : "error recuperando ususario"});
        } else {
          if (body.length > 0 &&  body[0].logged ){

              var putBody = '{"$unset":{"logged":""}}'
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(err, resMLab, body) {
                    console.log("Quitamos propiedad true ");
                    res.status(200);
                    res.send({"msg" : "usuario deslogado satisfactoriamente"});
                })
          }
          else {
            res.status(404);
            res.send({"msg" : "usuario no encontrado"});
          }

        }
               //res.send(response);
     }
     //body[0] mandamos el elemento 0 ya q solo recuperamos 1 registro


  )
} //function logoutV2(req, res){








function logout(req, res){
  console.log("post/apitechu/v1/logout");
  console.log("id es " + req.params.id);


  var existe = false;

  var users = require('../usuarios.json');
  var idUser = req.params.id;

  for (var i = 0; i < users.length; i++) {

    if (users[i].id == req.params.id) {
      console.log("El id recuperado para logout " + idUser);
      existe = true;

      if(users[i].logged==true){
          console.log("Si que está logado" );
        //hay que borrar la propiedad y salir
        delete users[i].logged;
        io.writeUserDataToFile(users);

        res.send({"msg" : "usuario deslogado correctamente" , "idUsuario": idUser});
        break;

    }
    else{
      res.send({"msg" : "usuario no logado"});
        break;
    }


    }
  }


}


module.exports.login = login;
module.exports.logout = logout;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
